# us 26 Individual Project 2

# Multiplication Web Application

This project is a simple web application built with Rust and the Actix web framework. It provides a user-friendly interface for entering a list of numbers and calculates their multiplication.

## Feature

1. **REST API Endpoint:** Processes JSON requests to calculate the multiplication of numbers.
2. **Interactive Web Interface:** Users can input a list of numbers to calculate their multiplication directly from their browser.

## Demo

Please refer to the demo video in the repository: 
![](Screen_Recording_2024-04-22_at_8.10.26_PM.mov)

## Author

Udyan Sachdev

## Project Structure

The project consists of a main Rust application that sets up an Actix web server with two endpoints:

1. A GET endpoint (`/`) that serves an HTML page. This page includes a form where users can input a list of numbers.
2. A POST endpoint (`/multiply`) that accepts JSON containing a list of numbers, calculates their multiplication, and returns the result in JSON format.

## Key Components

- **MultiplyRequest:** A struct that defines the expected format of JSON requests containing the list of numbers to multiply.
- **MultiplyResponse:** A struct that defines the format of JSON responses that provide the multiplication of the numbers.
- **multiply_numbers:** An asynchronous function that handles POST requests to the `/multiply` endpoint, calculates the multiplication of the provided numbers, and returns the result.
- **index:** An asynchronous function that serves the HTML for the web interface, enabling users to input their list of numbers and view the multiplication.

## Usage

After starting the web service, navigate to [http://localhost:8081](http://localhost:8081) in your web browser. Enter a list of numbers separated by commas in the input field (e.g., 1,2,3) and click the "Calculate the multiplication" button. The multiplication of the numbers will be displayed on the page.

## Configuration and Initialization

1. **Navigate to the project directory:**
    ```bash
    cd us26_microservice
    ```

2. **Add dependencies to Cargo.toml file and develop application code:**
    - Add dependencies to Cargo.toml file.
    - Develop application code in the src/main.rs file.

3. **Build and run:**
    ```bash
    cargo run
    ```

4. **Containerize the Rust Actix Web App:**
    - Create a Dockerfile in the project directory.
    - Run the following command in the same directory as your Dockerfile:
        ```bash
        docker build -t us26_microservice .
        ```

5. **Run the Container locally:**
    ```bash
    docker run -p 8081:8081 us26_microservice
    ```

6. **Access the webservice at [http://localhost:8081](http://localhost:8081).**
    
## Output 

### Building the container
![pic](screenshots/Screenshot 2024-03-24 at 11.16.41 PM.png)

![pic](screenshots/Screenshot 2024-03-24 at 11.19.17 PM.png)

### Local Host Output
![pic](screenshots/Screenshot 2024-03-24 at 11.45.42 PM.png)
