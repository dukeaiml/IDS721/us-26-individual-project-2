use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct MaxRequest {
    numbers: Vec<i32>,
}

#[derive(Serialize)]
struct MaxResponse {
    max_number: Option<i32>,
}

// Endpoint to find the maximum number from a list of numbers in a JSON request
async fn find_max_number(item: web::Json<MaxRequest>) -> impl Responder {
    let max_number = item.numbers.iter().cloned().max();
    web::Json(MaxResponse { max_number })
}  

// Endpoint to serve the HTML page
async fn index() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Maximum Number Finder</title>
        </head>
        <body>
            <h2>Add numbers separated by commas to find the maximum:</h2>
            <input type="text" id="numberList" placeholder="1,2,3 (for example)">
            <button onclick="findMax()">Find Maximum</button>
            <p id="result"></p>

            <script>
                async function findMax() {
                    const numbers = document.getElementById('numberList').value.split(',').map(Number);
                    const response = await fetch('/max', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({ numbers }),
                    });

                    if (response.ok) {
                        const data = await response.json();
                        document.getElementById('result').textContent = 'Maximum number in the list: ' + data.max_number;
                    } else {
                        document.getElementById('result').textContent = 'Error finding maximum number';
                    }
                }
            </script>
        </body>
        </html>
    "#;
    HttpResponse::Ok().content_type("text/html").body(html)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/max", web::post().to(find_max_number))
    })
    .bind("0.0.0.0:8081")?
    .run()
    .await
}
